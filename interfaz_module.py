from PyQt6 import uic
from PyQt6.QtWidgets import QApplication,QMainWindow,QGraphicsDropShadowEffect
from PyQt6.QtSerialPort import QSerialPortInfo
from PyQt6.QtGui import QColor
from PyQt6 import QtWidgets,QtCore
from PyQt6.QtCore import QPoint,QObject,pyqtSignal,QThread
import sys
import new_serial                   #modulo modificado para conexion con puerto serie
import subprocess                   #modulo para abrir plotjuggler
import datetime                     #modulo para el nombre del csv
import xml.etree.ElementTree as ET  #modulo para modificar layout de plotjuggler
import socket                       #modulo para servidor udp con plotjuggler
import msgpack                      #modulo para empaquetar el stream de datos
import time                         #modulo para sleep()
import platform                     #modulo para identificacion de sistema operativo
import os                           #modulo para check de path del puerto serie
from struct import pack, unpack             #funcion para transformar a bytes con formato
import json

class MyApp(QMainWindow) :

    enable_lectura_signal = pyqtSignal()
    disable_lectura_signal = pyqtSignal()
    enable_trigger_signal = pyqtSignal()
    disable_trigger_signal = pyqtSignal()
    enable_formato_signal = pyqtSignal()
    disable_formato_signal = pyqtSignal()
    enable_ref_signal = pyqtSignal()
    disable_ref_signal = pyqtSignal()
    enable_load_ref_signal = pyqtSignal()
    disable_load_ref_signal = pyqtSignal()
    toggle_stream_started = pyqtSignal()

    def __init__(self):
        super(MyApp,self).__init__()
        #Apertura del Qt designer
        uic.loadUi('interfaz_module.ui',self)

        #Comentario
        self.label_7.setText("  INTERFAZ")
        self.label_7.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 20pt \"Sitka Display\";")

        #Configuración botones de control
        self.Bt_normal.hide()
        self.click_posicion = QPoint()
        self.Bt_minimizar.clicked.connect(lambda: self.showMinimized())
        self.Bt_normal.clicked.connect(self.control_Bt_normal)
        self.Bt_max.clicked.connect(self.control_Bt_max)
        self.Bt_cerrar.clicked.connect(self.close)
        
        #Mover ventana
        self.frame_sup.mouseMoveEvent = self.Mover_ventana

        #Tamaño ventana
        self.gripsize = 10
        self.grip = QtWidgets.QSizeGrip(self)
        self.grip.resize(self.gripsize,self.gripsize)
        
        #Inicializa mascaras y valores de trigger y tiempo csv
        self.value_time_csv.setInputMask("99.99")
        self.value_trigger.setInputMask("9.99")
        self.value_time_csv.setText("02.00")
        self.value_trigger.setText("1.00")

        #Control Connect
        self.Bt_act.clicked.connect(self.read_ports)
        self.Bt_connect.clicked.connect(self.serial_connect)
        self.Bt_connect.clicked.connect(self.enable_formato_signal)
        self.Bt_disconnect.clicked.connect(self.serial_disconnect)
        self.Bt_disconnect.clicked.connect(self.disable_lectura_signal)
        self.Bt_disconnect.clicked.connect(self.disable_formato_signal)
        self.Bt_disconnect.clicked.connect(self.disable_trigger_signal)
        self.Btn_Cargar.clicked.connect(self.load_ref)
        self.Btn_Refresh.clicked.connect(self.refresh_ref)

        #Decoracion
        self.sombra_frame(self.Bt_act)

        #Elimino barra de titulos
        self.setWindowFlag(QtCore.Qt.WindowType.FramelessWindowHint)
        self.setWindowOpacity(1)

        #Configuración boton
        self.Btn_csv.toggled.connect(self.cambio_a_csv)
        self.Btn_streaming.toggled.connect(self.cambio_a_streaming)
        # self.Trigger_I.toggled.connect(self.I_btns_enable)
        # self.Trigger_U.toggled.connect(self.U_btns_enable)

        self.stream_started = False
        self.ctrl = {'break': False,
                     'ref_key': 0,
                     'ref_val': 0} #Control variable

        #Conexion de se;ales y slots
        self.enable_ref_signal.connect(self.ref_enable)
        self.disable_ref_signal.connect(self.ref_disable)
        self.enable_lectura_signal.connect(self.lectura_enable)
        self.disable_lectura_signal.connect(self.lectura_disable)
        self.enable_trigger_signal.connect(self.trigger_enable)
        self.disable_trigger_signal.connect(self.trigger_disable)
        self.enable_formato_signal.connect(self.formato_enable)
        self.disable_formato_signal.connect(self.formato_disable)
        self.enable_load_ref_signal.connect(self.ref_load_enable)
        self.disable_load_ref_signal.connect(self.ref_load_disable)
        self.toggle_stream_started.connect(self.__stream_started__)

        #habilitar el boton de cargar cuando alguno de las referencias se selecciona
        self.Btn_Iscref.clicked.connect(self.ref_load_enable)
        self.Btn_Ibatref.clicked.connect(self.ref_load_enable)
        self.Btn_Vbusref.clicked.connect(self.ref_load_enable)
        self.Btn_Vscref.clicked.connect(self.ref_load_enable)

        #########################
        self.checkBox_Isc.stateChanged.connect(self.command_number)
        self.checkBox_Ibat.stateChanged.connect(self.command_number)
        self.checkBox_Vbus.stateChanged.connect(self.command_number)
        self.checkBox_Vsc.stateChanged.connect(self.command_number)        
        #########################

        self.value_ref.setDecimals(3)
        self.value_ref.setSingleStep(0.005)
            
    def cambio_a_csv(self):
        if self.Btn_csv.isChecked():
            self.enable_trigger_signal.emit()
            self.enable_lectura_signal.emit()

            try: self.Btn_Comando.clicked.disconnect() 
            except Exception: pass

            self.Btn_Comando.clicked.connect(self.csv_function)
              
    def cambio_a_streaming(self) :
        if self.Btn_streaming.isChecked():
            self.disable_trigger_signal.emit()
            self.enable_lectura_signal.emit()

            try: self.Btn_Comando.clicked.disconnect() 
            except Exception: pass

            self.Btn_Comando.clicked.connect(self.Thread_streaming)


    def read_ports(self) :
        self.baudrates = ['1200', '1800', '2400', '4800',
                        '9600', '19200', '38400', '57600', '115200', '250000', '500000','921600']
        
        portList = []
        ports = QSerialPortInfo().availablePorts()
        for i in ports :
            portList.append(i.portName())
        
        self.cb_list_ports.clear()
        self.cb_list_baudrates.clear()
        self.cb_list_ports.addItems(portList)
        self.cb_list_baudrates.addItems(self.baudrates)
        self.cb_list_baudrates.setCurrentText(self.baudrates[-1])
        self.label_7.setText("Se han actualizado los puertos con éxito")
        self.label_7.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 10pt \"Sitka Display\";")
        self.Bt_connect.setEnabled(True)
        self.Bt_connect.setStyleSheet(u"background-color : rgb(243,202,76);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.sombra_frame(self.Bt_connect)
    
    def serial_connect(self):
        self.port = self.cb_list_ports.currentText()
        self.baud = self.cb_list_baudrates.currentText()
        if platform.system()=='Linux':
            self.port = "/dev/" + self.port

        
        if os.path.exists(self.port):
            self.serial_object = new_serial.New_Serial(self.port,self.baud,bytesize=8,stopbits=1)
            time.sleep(2)
            self.label_7.setText("Se ha conectado correctamente al puerto : " + str(self.port))
            self.label_7.setStyleSheet(u"color : rgb(0,255,0);\n"
            "font : 87 10pt \"Sitka Display\";")
            self.Btn_stop.clicked.connect(self.btn_stop)
            self.Btn_stop.clicked.connect(lambda: self.csv_function)  #Aca modifique. self.csv_function(0)--> self.csv_function
            self.enable_ref_signal.emit()
            self.Bt_disconnect.setEnabled(True)
            self.Bt_disconnect.setStyleSheet(u"background-color : rgb(243,202,76);\n"
            "font : 700 16pt \"Sitka Display\";")
            self.sombra_frame(self.Bt_disconnect)
            self.Bt_act.setEnabled(False)
            self.Bt_act.setStyleSheet(u"background-color : rgb(100,100,100);\n"
            "font : 700 16pt \"Sitka Display\";")
            self.sombra_frame_off(self.Bt_act)
            self.Bt_connect.setEnabled(False)
            self.Bt_connect.setStyleSheet(u"background-color : rgb(100,100,100);\n"
            "font : 700 16pt \"Sitka Display\";")
            self.sombra_frame_off(self.Bt_connect)
            if self.Btn_csv.isChecked():
                self.cambio_a_csv()
            elif self.Btn_streaming.isChecked():
                self.cambio_a_streaming()
        else:
            self.label_7.setText("No se encontro el puerto : " + str(self.port))
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
            print("no se encontro el puerto serie")

    def serial_disconnect(self):
        self.serial_object.close()
        self.disable_ref_signal.emit()
        self.disable_load_ref_signal.emit()
        self.label_7.setText("Se ha desconectado correctamente del puerto : " + str(self.port))
        self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
        "font : 87 10pt \"Sitka Display\";")
        self.Bt_connect.setEnabled(True)
        self.Bt_connect.setStyleSheet(u"background-color : rgb(243,202,76);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.sombra_frame(self.Bt_connect)
        self.Bt_act.setEnabled(True)
        self.Bt_act.setStyleSheet(u"background-color : rgb(243,202,76);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.sombra_frame(self.Bt_act)
        self.Bt_disconnect.setEnabled(False)
        self.Bt_disconnect.setStyleSheet(u"background-color : rgb(100,100,100);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.sombra_frame_off(self.Bt_disconnect)

    def lectura_enable(self):
        self.label_8.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 700 20pt \"Sitka Display\";")
        # self.sombra_frame(self.Btn_1)
        # self.sombra_frame(self.Btn_2)
        # self.sombra_frame(self.Btn_3)
        # self.sombra_frame(self.Btn_4)
        self.Btn_Comando.setEnabled(True)
        self.checkBox_Isc.setEnabled(True)
        self.checkBox_Ibat.setEnabled(True)
        self.checkBox_Vbus.setEnabled(True)
        self.checkBox_Vsc.setEnabled(True)
        self.Btn_Comando.setStyleSheet(u"background-color : rgb(243,202,76);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.checkBox_Isc.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.checkBox_Ibat.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.checkBox_Vbus.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.checkBox_Vsc.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        
    def lectura_disable(self):
        self.label_8.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 700 20pt \"Sitka Display\";")
        # self.sombra_frame_off(self.Btn_1)
        # self.sombra_frame_off(self.Btn_2)
        # self.sombra_frame_off(self.Btn_3)
        # self.sombra_frame_off(self.Btn_4)
        self.Btn_Comando.setEnabled(False)
        self.checkBox_Isc.setEnabled(False)
        self.checkBox_Ibat.setEnabled(False)
        self.checkBox_Vbus.setEnabled(False)
        self.checkBox_Vsc.setEnabled(False)
        self.Btn_Comando.setStyleSheet(u"background-color : rgb(100,100,100);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.checkBox_Isc.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.checkBox_Ibat.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.checkBox_Vbus.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.checkBox_Vsc.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")

    def trigger_enable(self):
        self.label_2.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.label_3.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.label_6.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.value_time_csv.setEnabled(True)
        self.value_trigger.setEnabled(True)
        self.Trigger_I.setEnabled(True)
        self.Trigger_U.setEnabled(True)
        self.Btn_subida.setEnabled(True)
        self.Btn_bajada.setEnabled(True)
        self.Trigger_I.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Trigger_U.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_subida.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_bajada.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.value_time_csv.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.value_trigger.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")

    def trigger_disable(self):
        self.label_2.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.label_3.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.label_6.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.value_time_csv.setEnabled(False)
        self.value_trigger.setEnabled(False)
        self.Trigger_I.setEnabled(False)
        self.Trigger_U.setEnabled(False)
        self.Btn_subida.setEnabled(False)
        self.Btn_bajada.setEnabled(False)
        self.Trigger_I.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Trigger_U.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_subida.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_bajada.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.value_time_csv.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.value_trigger.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")

    def formato_enable(self):
        self.label_4.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.Btn_csv.setEnabled(True)
        self.Btn_streaming.setEnabled(True)
        self.Btn_csv.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_streaming.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_stop.setEnabled(True)
        self.sombra_frame(self.Btn_stop)
        self.Btn_stop.setStyleSheet(u"background-color : rgb(243,202,76);\n"
        "font : 700 16pt \"Sitka Display\";")

    def formato_disable(self):
        self.label_4.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.Btn_csv.setEnabled(False)
        self.Btn_streaming.setEnabled(False)
        self.Btn_csv.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_streaming.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_stop.setEnabled(False)
        self.sombra_frame_off(self.Btn_stop)
        self.Btn_stop.setStyleSheet(u"background-color : rgb(100,100,100);\n"
        "font : 700 16pt \"Sitka Display\";")

    def ref_enable(self):
        self.label_9.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.Btn_Iscref.setEnabled(True)
        self.Btn_Ibatref.setEnabled(True)
        self.Btn_Vbusref.setEnabled(True)
        self.Btn_Vscref.setEnabled(True)
        self.Btn_Iscref.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_Ibatref.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_Vbusref.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_Vscref.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")


    def ref_disable(self):
        self.label_9.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 700 20pt \"Sitka Display\";")
        self.Btn_Iscref.setEnabled(False)
        self.Btn_Ibatref.setEnabled(False)
        self.Btn_Vbusref.setEnabled(False)
        self.Btn_Vscref.setEnabled(False)
        self.Btn_Iscref.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_Ibatref.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_Vbusref.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_Vscref.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
    
    def ref_load_disable(self):
        self.value_ref.setEnabled(False)
        self.Btn_Cargar.setEnabled(False)
        self.Btn_Refresh.setEnabled(False)
        self.Btn_Refresh.setStyleSheet(u"background-color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")
        self.Btn_Cargar.setStyleSheet(u"background-color : rgb(100,100,100);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.value_ref.setStyleSheet(u"color : rgb(100,100,100);\n"
        "font : 87 16pt \"Sitka Display\";")

    def ref_load_enable(self):
        self.value_ref.setEnabled(True)
        self.Btn_Cargar.setEnabled(True)
        self.Btn_Refresh.setEnabled(True)
        self.Btn_Cargar.setStyleSheet(u"background-color : rgb(243,202,76);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.Btn_Refresh.setStyleSheet(u"background-color : rgb(243,202,76);\n"
        "font : 700 16pt \"Sitka Display\";")
        self.value_ref.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 16pt \"Sitka Display\";")

    def btn_stop(self):
        self.serial_object.write(pack('B',0))
        self.toggle_stream_started.emit()
        self.ctrl['break'] = True
        self.checkBox_Isc.setChecked(False)
        self.checkBox_Ibat.setChecked(False)
        self.checkBox_Vbus.setChecked(False)
        self.checkBox_Vsc.setChecked(False)
        self.enable_lectura_signal.emit()

    #-----------------------------------------------------------------------------------    
    def sombra_frame(self, frame):
        sombra = QGraphicsDropShadowEffect(self)
        sombra.setBlurRadius(30)
        sombra.setXOffset(0)
        sombra.setYOffset(0)
        sombra.setColor(QColor(243,202,76,255))
        frame.setGraphicsEffect(sombra)
    
    def sombra_frame_off(self, frame):
        sombra = QGraphicsDropShadowEffect(self)
        sombra.setBlurRadius(0)
        sombra.setXOffset(0)
        sombra.setYOffset(0)
        sombra.setColor(QColor(243,202,76,0))
        frame.setGraphicsEffect(sombra)

    def control_Bt_normal(self):
        self.showNormal()
        self.Bt_normal.hide()
        self.Bt_max.show()
    
    def control_Bt_max(self) :
        self.showMaximized()
        self.Bt_max.hide()
        self.Bt_normal.show()

    def resizeEvent(self,event):
        rect = self.rect()
        self.grip.move(rect.right() - self.gripsize,rect.bottom()- self.gripsize)

    #Mover ventana
    def mousePressEvent(self, event) :
        self.click_posicion = event.globalPosition().toPoint()

    def Mover_ventana(self, event):
        if self.isMaximized() == False : 
            if event.buttons() == QtCore.Qt.MouseButton.LeftButton:
                self.move(self.pos() + event.globalPosition().toPoint() - self.click_posicion)
                self.click_posicion = event.globalPosition().toPoint()
                event.accept()
        if event.globalPosition().toPoint().y() <= 10 :
            self.showMaximized()
            self.Bt_max.hide()
            self.Bt_normal.show()
        else :
            self.showNormal()
            self.Bt_normal.hide()
            self.Bt_max.show()

    def load_ref(self):
        if self.Btn_Iscref.isChecked():
            print("Loading Isc ref")
            self.serial_object.write(pack('B',8))
        elif self.Btn_Ibatref.isChecked():
            print("Loading Ibat ref")
            self.serial_object.write(pack('B',9))
        elif self.Btn_Vbusref.isChecked():
            print("Loading Vbus ref")
            self.serial_object.write(pack('B',11))
        elif self.Btn_Vscref.isChecked():
            print("Loading Vsc ref")
            self.serial_object.write(pack('B',10))

        valor_escalonado = self.value_ref.value() * (2**20)
        valor_clampeado = max(min(valor_escalonado, (2 ** (32 - 1)) - 1), -(2 ** (32 - 1)))
        valor_bytes = pack('>i',int(valor_clampeado))
        for byte in valor_bytes:
            byte_valor_clampeado = bytes([byte])
            self.serial_object.write(byte_valor_clampeado)
            print(byte_valor_clampeado)

    def __stream_started__(self):
        self.stream_started = not self.stream_started

    def refresh_ref(self):
        #Deshabilita Btn refresh
        
        if self.Btn_Iscref.isChecked():
            print("Refreshing I1 ref")
            self.serial_object.write(pack('B',12))
            wait = 12
        elif self.Btn_Ibatref.isChecked():
            print("Refreshing I2 ref")
            self.serial_object.write(pack('B',13))
            wait = 13
        elif self.Btn_Vbusref.isChecked():
            print("Refreshing V1 ref")
            self.serial_object.write(pack('B',15))
            wait = 15
        elif self.Btn_Vscref.isChecked():
            print("Refreshing V2 ref")
            self.serial_object.write(pack('B',14))
            wait = 14
        
        if self.stream_started:
            self.ctrl['ref_key'] = wait

            while self.ctrl['ref_key'] == wait:
                self.value_ref.setValue(self.ctrl['ref_val'])
                if self.ctrl['ref_key'] == wait:
                    print(self.ctrl['ref_key'])
            return
        else:
            key=0
            len=0
            while (key != wait):
                #self.serial_object.reset_input_buffer()
                header = self.serial_object.read_header()
                key = header[0]
                len = header[1]

            valor_bytes = b''
            for i in range(len):#range(4):
                byte = self.serial_object.read()
                valor_bytes += byte

            entero = int.from_bytes(valor_bytes, byteorder='big')
            float_value = entero / (2**20)
            self.value_ref.setValue(float_value)
            #print(valor_bytes)
            #print(float_value)
            #Habilita boton refresh

    def command_number(self):
        print("estoy en command number")
        if self.checkBox_Isc.isChecked() and self.checkBox_Ibat.isChecked() and self.checkBox_Vbus.isChecked() and self.checkBox_Vsc.isChecked():
            self.comando = 7
            self.label_7.setText("comando valido")
            self.label_7.setStyleSheet(u"color : rgb(0,255,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Isc.isChecked() and self.checkBox_Ibat.isChecked() and self.checkBox_Vbus.isChecked():
            self.comando = 0
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Isc.isChecked() and self.checkBox_Ibat.isChecked() and self.checkBox_Vsc.isChecked():
            self.comando = 0
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Isc.isChecked() and self.checkBox_Vbus.isChecked() and self.checkBox_Vsc.isChecked():
            self.comando = 0
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Ibat.isChecked() and self.checkBox_Vbus.isChecked() and self.checkBox_Vsc.isChecked():
            self.comando = 0
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Isc.isChecked() and self.checkBox_Ibat.isChecked():
            self.comando = 0
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Isc.isChecked() and self.checkBox_Vbus.isChecked():
            self.comando = 5
            self.label_7.setText("comando valido")
            self.label_7.setStyleSheet(u"color : rgb(0,255,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Isc.isChecked() and self.checkBox_Vsc.isChecked():
            self.comando = 0
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Ibat.isChecked() and self.checkBox_Vbus.isChecked():
            self.comando = 0
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Ibat.isChecked() and self.checkBox_Vsc.isChecked():
            self.comando = 6
            self.label_7.setText("comando valido")
            self.label_7.setStyleSheet(u"color : rgb(0,255,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Vbus.isChecked() and self.checkBox_Vsc.isChecked():
            self.comando = 0
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Isc.isChecked():
            self.comando = 1
            self.label_7.setText("comando valido")
            self.label_7.setStyleSheet(u"color : rgb(0,255,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Ibat.isChecked():
            self.comando = 2
            self.label_7.setText("comando valido")
            self.label_7.setStyleSheet(u"color : rgb(0,255,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Vbus.isChecked():
            self.comando = 3
            self.label_7.setText("comando valido")
            self.label_7.setStyleSheet(u"color : rgb(0,255,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        elif self.checkBox_Vsc.isChecked():
            self.comando = 4
            self.label_7.setText("comando valido")
            self.label_7.setStyleSheet(u"color : rgb(0,255,0);\n"
            "font : 87 10pt \"Sitka Display\";")
        else:
            print("comando vale:")
            self.comando = 0
            print(self.comando)
            self.label_7.setText("comando no valido")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
            return

    def csv_function(self):
        # Mandar self.comando por puerto serie
        # self.command_number() # actualizo el valor de self.comando
        #print("Sending command: " + str(self.comando))
        self.serial_object.write(pack('B',self.comando))

        if self.comando == 0: # Si el self.comando es parada, salir de la funcion
            return
        
        if self.trigger() == -1:
            return
        
        self.Time=0
        self.Time_count=0
        flag=1

        # using now() to get current time
        current_time = datetime.datetime.now()
        csv_name=str(current_time.year) + '-' +str(current_time.month)+'-'+str(current_time.day)+'-'+str(current_time.hour)+'_'+str(current_time.minute)+'_'+str(current_time.second)+'.csv'
        try:
            with open(csv_name, "w+") as f:
                while self.Time < float(self.value_time_csv.text()):
                    self.serial_object.reset_input_buffer()
                    header = self.serial_object.read_header()
                    self.key = header[0]
                    if (self.key == 7) & (flag == 1):
                        flag=0
                        f.write("Tiempo" + ',' + "Corriente de SC" + ',' + "Corriente de BL" + ',' + "Tension de bus" + ',' + "Tension de SC" + '\n')
                        self.xml_edit(csv_name,"Corriente de BL","Tension de SC")
                    elif (self.key == 5) & (flag == 1):
                        flag=0
                        f.write("Tiempo" + ',' + "Corriente de SC" + ',' + "Tension de bus" + '\n')
                        self.xml_edit(csv_name,"Corriente de BL","Tension de SC")
                    elif (self.key == 6) & (flag == 1):
                        flag=0
                        f.write("Tiempo" + ',' + "Corriente de BL" + ',' + "Tension de SC" + '\n')
                        self.xml_edit(csv_name,"Corriente de BL","Tension de SC")
                    elif (self.key == 1) & (flag == 1):
                        flag=0
                        f.write("Tiempo" + ',' + "Corriente de SC" + '\n')
                        self.xml_edit(csv_name,"Corriente de SC")
                    elif (self.key == 2) & (flag == 1):
                        flag=0
                        f.write("Tiempo" + ',' + "Corriente de BL" + '\n')
                        self.xml_edit(csv_name,"Corriente de BL")
                    elif (self.key == 3) & (flag == 1):
                        flag=0
                        f.write("Tiempo" + ',' + "Tension de bus" + '\n')
                        self.xml_edit(csv_name,"Tension de bus")
                    elif (self.key == 4) & (flag == 1):
                        flag=0
                        f.write("Tiempo" + ',' + "Tension de SC" + '\n')
                        self.xml_edit(csv_name,"Tension de SC")

                    length = header[1]
                    self.read_data(f,length)
            f.close()
            self.serial_object.write(pack('B',0))

        except FileNotFoundError:
            self.serial_object.write(pack('B',0))
            print("The 'docs' directory does not exist")
        
        self.label_7.setText("Se ha creado el archivo csv: " + csv_name)
        self.label_7.setStyleSheet(u"color : rgb(243,202,76);\n"
        "font : 87 10pt \"Sitka Display\";")

        if self.comando != 5 and self.comando != 6:
            if platform.system()== 'Linux' :
                with subprocess.Popen("plotjuggler -l layout.xml", stdout=subprocess.PIPE,shell=True) as proc:
                       print(proc.stdout.read())
            else :
                subprocess.run(["C:\Program Files\PlotJuggler\plotjuggler.exe","-l","layout.xml"])
        else:
            if platform.system()== 'Linux' :
                with subprocess.Popen("plotjuggler -l layout_2.xml", stdout=subprocess.PIPE,shell=True) as proc:
                    print(proc.stdout.read())
            else :
                subprocess.run(["C:\Program Files\PlotJuggler\plotjuggler.exe","-l","layout_2.xml"])
    
    def conv_Isc(self,dato):
        return -0.009107293454962 * float(dato) + (18.983079680355484 + 0.2)
    
    def conv_Ibat(self,dato):
        return -0.008971907544623 * float(dato) + (18.733570241497013 + 0.275)
    
    def conv_Vbus(self,dato):
        return 0.021136390543381 * float(dato) + 0.078517163987334
    
    def conv_Vsc(self,dato):
        return 0.007465131795654 * float(dato) - 0.254899031191860
        
    def read_data(self,f,length):
        f.write(f'{self.Time:.4f}')
        for i in range(length // 2):
            dato = int.from_bytes(self.serial_object.read(2),byteorder= 'big')
            if (self.key == 1):
                Isc = self.conv_Isc(dato)
                f.write(',' + f'{Isc:.4f}')
            elif (self.key == 2):
                Ibat = self.conv_Ibat(dato)
                f.write(',' + f'{Ibat:.4f}')
            elif (self.key == 3):
                Vbus = self.conv_Vbus(dato)
                f.write(',' + f'{Vbus:.4f}')
            elif (self.key == 4):
                Vsc = self.conv_Vsc(dato)
                f.write(',' + f'{Vsc:.4f}')
            elif (self.key == 5):
                if(i == 0):
                    Isc = self.conv_Isc(dato)
                    f.write(',' + f'{Isc:.4f}')
                elif(i == 1):
                    Vbus = self.conv_Vbus(dato)
                    f.write(',' + f'{Vbus:.4f}')
            elif (self.key == 6):
                if(i == 0):
                    Ibat = self.conv_Ibat(dato)
                    f.write(',' + f'{Ibat:.4f}')
                elif(i == 1):
                    Vsc = self.conv_Vsc(dato)
                    f.write(',' + f'{Vsc:.4f}')
            elif (self.key == 7):
                if(i == 0):
                    Isc = self.conv_Isc(dato)
                    f.write(',' + f'{Isc:.4f}')
                elif(i == 1):
                    Ibat = self.conv_Ibat(dato)
                    f.write(',' + f'{Ibat:.4f}')
                elif(i == 2):
                    Vbus = self.conv_Vbus(dato)
                    f.write(',' + f'{Vbus:.4f}')
                elif(i == 3):
                    Vsc = self.conv_Vsc(dato)
                    f.write(',' + f'{Vsc:.4f}')
        f.write('\n')
        self.Time+=((2*((length // 2)+1))/70892.3)*100
        print(self.Time)

        
    def trigger(self):
        if self.Btn_subida.isChecked() == False and self.Btn_bajada.isChecked() == False:
            self.label_7.setText("Elegir flanco para el trigger")
            self.label_7.setStyleSheet(u"color : rgb(255,0,0);\n"
            "font : 87 10pt \"Sitka Display\";")
            return -1
        
        dato_trigger=[0,0]
        cc = self.serial_object.read_header()
        self.key = cc[0]
        length = cc[1]
        
        if length == 4:
            dato = int.from_bytes(self.serial_object.read(2),byteorder= 'big')
            if (self.key == 5):
                dato = self.conv_Isc(dato)
            elif (self.key == 6):
                dato = self.conv_Ibat(dato)
                dato2 = int.from_bytes(self.serial_object.read(2),byteorder= 'big')
                dato2 = self.conv_Vsc(dato2)

            dato2 = int.from_bytes(self.serial_object.read(2),byteorder= 'big')
            
            if (self.key == 5):
                dato2 = self.conv_Vbus(dato2)
            elif (self.key == 6):
                dato2 = self.conv_Vsc(dato2)
                
            if self.Trigger_I.isChecked():
                #print(dato)
                dato_trigger[0]=dato
                dato_trigger[-1]=dato_trigger[0]
            elif self.Trigger_U.isChecked():
                #print(dato)
                dato_trigger[0]=dato2
                dato_trigger[-1]=dato_trigger[0]
        elif length == 2:
            dato_trigger[0] = int.from_bytes(self.serial_object.read(length),byteorder= 'big')
            if (self.key == 1):
                dato_trigger[0] = self.conv_Isc(dato_trigger[0])
            elif (self.key == 2):
                dato_trigger[0] = self.conv_Ibat(dato_trigger[0])           
            elif (self.key == 3):
                dato_trigger[0] = self.conv_Vbus(dato_trigger[0])            
            elif (self.key == 4):
                dato_trigger[0] = self.conv_Vsc(dato_trigger[0])           
            
            #print(dato_trigger[0])
            dato_trigger[-1]=dato_trigger[0]
        
        delta = 0
        trigger_armed = False
        while True:
            self.serial_object.reset_input_buffer()
            cc = self.serial_object.read_header()
            self.key = cc[0]
            length = cc[1]
            if length == 4:
                dato = int.from_bytes(self.serial_object.read(2),byteorder= 'big')
                #print(self.key)
                if (self.key == 5):
                    dato = self.conv_Isc(dato)
                elif (self.key == 6):
                    dato = self.conv_Ibat(dato)

                dato2 = int.from_bytes(self.serial_object.read(2),byteorder= 'big')
                
                if (self.key == 5):
                    dato2 = self.conv_Vbus(dato2)
                elif (self.key == 6):
                    dato2 = self.conv_Vsc(dato2)

                if self.Trigger_I.isChecked():
                    dato_trigger[0]=dato
                elif self.Trigger_U.isChecked():
                    dato_trigger[0]=dato2

            elif length == 2:
                dato_trigger[0] = int.from_bytes(self.serial_object.read(length),byteorder= 'big')
                if (self.key == 1):
                    dato_trigger[0] = self.conv_Isc(dato_trigger[0])
                elif (self.key == 2):
                    dato_trigger[0] = self.conv_Ibat(dato_trigger[0])           
                elif (self.key == 3):
                    dato_trigger[0] = self.conv_Vbus(dato_trigger[0])            
                elif (self.key == 4):
                    dato_trigger[0] = self.conv_Vsc(dato_trigger[0])
            
            if abs(dato_trigger[0] - dato_trigger[-1]) > 0.05:
                delta = dato_trigger[0] - dato_trigger[-1]
                dato_trigger[-1]=dato_trigger[0]
            
            #print("valor " + str(dato_trigger[0]) + " y delta vale " + str(delta))
            if self.Btn_subida.isChecked() and dato_trigger[0] < float(self.value_trigger.text()):
                print("trigger armed")
                trigger_armed = True
            elif self.Btn_bajada.isChecked() and dato_trigger[0] > float(self.value_trigger.text()):
                trigger_armed = True
            
            if trigger_armed:
                if self.Btn_subida.isChecked() and delta > 0 and dato_trigger[0] > float(self.value_trigger.text()):
                    return
                elif self.Btn_bajada.isChecked() and delta < 0 and dato_trigger[0] < float(self.value_trigger.text()):
                    return
    
    def xml_edit(self,file_name,colum_name,colum_name2=None):
        if colum_name2 is not None:
            tree = ET.parse('layout_2.xml')
        else:
            tree = ET.parse('layout.xml')
        
        root = tree.getroot()

        for i in root.iter('fileInfo'):
            i.set('filename',file_name)

        curvas=list(root.iter('curve'))
        curvas[0].set('name',colum_name)

        if colum_name2 is not None:
            curvas[1].set('name',colum_name2)
            tree.write('layout_2.xml', "UTF-8")
            return
        else:
            tree.write('layout.xml', "UTF-8")
            return
    
    def Thread_streaming(self):

        # self.command_number() # actualizo el valor de self.comando
        # Step 2: Create a QThread object
        self.toggle_stream_started.emit()
        self.thread = QThread()
        # Step 3: Create a worker object
        self.worker = Worker_DataStreaming(self.comando,self.serial_object,self.ctrl)
        # Step 4: Move worker to the thread
        self.worker.moveToThread(self.thread)
        # Step 5: Connect signals and slots
        self.thread.started.connect(self.worker.DataStreaming)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        # Step 6: Start the thread
        print("starting thread")
        self.thread.start()
        print("thread started")

        self.disable_lectura_signal.emit() #Cuando se empiezan a mandar datos por udp se deshabilitan comandos de lectura.
        
        # with subprocess.Popen("plotjuggler", stdout=subprocess.PIPE,shell=True) as proc:
        #     print(proc.stdout.read())

        # Final resets
        self.thread.finished.connect(self.lectura_enable) #Cuando finaliza el hilo se habilitan los comandos de lectura. 
        return

# Step 1: Create a worker class
class Worker_DataStreaming(QObject):
    def __init__(self, comando, serial_object,ctrl):
        super().__init__()
        self.comando = comando
        self.serial_object = serial_object
        self.ctrl = ctrl

    finished = pyqtSignal()

    def conv_Isc(self,dato):
        return -0.009107293454962 * dato + (18.983079680355484 + 0.2)
    
    def conv_Ibat(self,dato):
        return -0.008971907544623 * dato + (18.733570241497013 + 0.275)
    
    def conv_Vbus(self,dato):
        return 0.021136390543381 * dato * (46.5/48) + 0.078517163987334
    
    def conv_Vsc(self,dato):
        return 0.007465131795654 * dato - 0.254899031191860

    def DataStreaming(self):
        UDP_IP = "127.0.0.1"
        UDP_PORT = 1234
        
        print ("UDP target IP:", UDP_IP)
        print ("UDP target port:",UDP_PORT)
        
        UDPServerSocket = socket.socket(socket.AF_INET, # Internet
                             socket.SOCK_DGRAM) # UDP
        
        print("UDP server up and sending messeges")
        if self.comando == 1:
            data_name = ["Corriente de SC"]
        elif self.comando == 2:
            data_name = ["Corriente de BL"]
        elif self.comando == 3:
            data_name = ["Tension de bus"]
        elif self.comando == 4:
            data_name = ["Tension de SC"]
        elif self.comando == 5:
            data_name = ["Corriente de SC","Tension de bus"]
        elif self.comando == 6:
            data_name = ["Corriente de BL","Tension de SC"]
        elif self.comando == 7:
            data_name = ["Corriente de SC","Corriente de BL","Tension de bus","Tension de SC"]
            print("comando 7")
        
        self.Time = 0
        self.serial_object.write(pack('B',self.comando))
        print(self.comando)
        self.ctrl['break'] = False
        self.ctrl['ref_key'] = 0
        while self.ctrl['break'] is not True:
            self.serial_object.reset_input_buffer()
            cc = self.serial_object.read_header()
            self.key = cc[0]
            length = cc[1]
            form_json = self.read_data_streaming(data_name,length)
            str_msgpackb_bytes=msgpack.packb(form_json)        
            UDPServerSocket.sendto(str_msgpackb_bytes, (UDP_IP,UDP_PORT))
            #print("control key vale: " + str(self.ctrl['ref_key']) + " y key vale: " + str(self.key))
            while self.ctrl['ref_key'] != 0:
                self.key=0
                len=0
                while (self.key != self.ctrl['ref_key']):
                    header = self.serial_object.read_header()
                    self.key = header[0]
                    len = header[1]
                    

                # self.serial_object.reset_input_buffer()

                valor_bytes = b''
                for i in range(len):#range(4):
                    byte = self.serial_object.read()
                    valor_bytes += byte

                entero = int.from_bytes(valor_bytes, byteorder='big')
                self.ctrl['ref_val'] = entero / (2**20)
                print(valor_bytes)
                print(self.ctrl['ref_val'])
                self.ctrl['ref_key'] = 0

        print("fin del thread")
        self.finished.emit()

    def read_data_streaming(self,data_name,length):
        formato_json = json.dumps({"timestamp":self.Time})
        formato_json_i = json.loads(formato_json)
        for i in range(length // 2):#range(len(data_name)): #range(length // 2):
            self.Time +=(2*((length // 2)+1))/92160
            #for i in range(length // 2):
            dato = int.from_bytes(self.serial_object.read(2),byteorder= 'big')
            if (self.key == 1):
                Isc = self.conv_Isc(dato)
                try:
                    formato_json_i.update({data_name[i] : Isc})
                except (IndexError):
                    print("Error index")
            elif (self.key == 2):
                Ibat = self.conv_Ibat(dato)
                try:
                    formato_json_i.update({data_name[i] : Ibat})
                except (IndexError):
                    print("Error index")
            elif (self.key == 3):
                Vbus = self.conv_Vbus(dato)
                try:
                    formato_json_i.update({data_name[i] : Vbus})
                except (IndexError):
                    print("Error index")
            elif (self.key == 4):
                Vsc = self.conv_Vsc(dato)
                try:
                    formato_json_i.update({data_name[i] : Vsc})
                except (IndexError):
                    print("Error index")
            elif (self.key == 5):
                if(i == 0):
                    Isc = self.conv_Isc(dato)
                    try:
                        formato_json_i.update({data_name[i] : Isc})
                    except (IndexError):
                        print("Error index")
                elif(i == 1):
                    Vbus = self.conv_Vbus(dato)
                    try:
                        formato_json_i.update({data_name[i] : Vbus})
                    except (IndexError):
                        print("Error index")
            elif (self.key == 6):
                if(i == 0):
                    Ibat = self.conv_Ibat(dato)
                    try:
                        formato_json_i.update({data_name[i] : Ibat})
                    except (IndexError):
                        print("Error index")
                elif(i == 1):
                    Vsc = self.conv_Vsc(dato)
                    try:
                        formato_json_i.update({data_name[i] : Vsc})
                    except (IndexError):
                        print("Error index")
            elif (self.key == 7):
                if(i == 0):
                    Isc = self.conv_Isc(dato)
                    try:
                        formato_json_i.update({data_name[i] : Isc})
                    except (IndexError):
                        print("Error index")
                elif(i == 1):
                    Ibat = self.conv_Ibat(dato)
                    try:
                        formato_json_i.update({data_name[i] : Ibat})
                    except (IndexError):
                        print("Error index")
                elif(i == 2):
                    Vbus = self.conv_Vbus(dato)
                    try:
                        formato_json_i.update({data_name[i] : Vbus})
                    except (IndexError):
                        print("Error index")
                elif(i == 3):
                    Vsc = self.conv_Vsc(dato)
                    try:
                        formato_json_i.update({data_name[i] : Vsc})
                    except (IndexError):
                        print("Error index")
        return formato_json_i
    
if __name__ == "__main__":
    App = QApplication(sys.argv)
    my_app = MyApp()
    my_app.show()

    try :
        sys.exit(App.exec())
    except SystemExit :
        if my_app.serial_object:
            my_app.serial_object.close()
        print("Close Windows...")