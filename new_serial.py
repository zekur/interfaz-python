import serial
from serial import Serial

class New_Serial(Serial):
    def __init__(self,
                port=None,
                baudrate=9600,
                bytesize=serial.EIGHTBITS,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                timeout=None,
                xonxoff=False,
                rtscts=False,
                write_timeout=None,
                dsrdtr=False,
                inter_byte_timeout=None,
                exclusive=None
                ):
        super().__init__(
                port=port,
                baudrate=baudrate,
                bytesize=bytesize,
                parity=parity,
                stopbits=stopbits,
                timeout=timeout,
                xonxoff=xonxoff,
                rtscts=rtscts,
                write_timeout=write_timeout,
                dsrdtr=dsrdtr,
                inter_byte_timeout=inter_byte_timeout,
                exclusive=exclusive
                )
    
    def read_header(self):
        limite_inf = b'\xFF\x00'
        limite_sup = b'\xFF\xFF'
        size = 2
        line = bytearray()
        arr = []
        while True:
            c = self.read(1)
            if c:
                line += c
                if size is not None and len(line) > size:
                    line.pop(0)
                if ((int.from_bytes(line[-size:],byteorder= 'big') > int.from_bytes(limite_inf,byteorder= 'big')) and 
                    (int.from_bytes(line[-size:],byteorder= 'big') < int.from_bytes(limite_sup,byteorder= 'big'))):
                    arr.append(self.extract_key(line))
                    arr.append(self.extract_length(line))
                    break
            else:
                break
        return arr
    
    def extract_key(self,line) -> int:
        return int((line[-1] >> 4))

    def extract_length(self,line) -> int:
        return int(line[-1] & 0x0F)

